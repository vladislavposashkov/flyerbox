package com.flyerbox.util;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.security.SecureRandom;


public class PasswordUtil {

    private static final int SALT_LENGTH = 50;

    public static String createSalt() {
        byte[] bytes = new byte[SALT_LENGTH];
        SecureRandom random = new SecureRandom();
        random.nextBytes(bytes);
        return Hex.encodeHexString(bytes);
    }

    public static String createHash(String password, String salt) {
        return DigestUtils.md5Hex(password.concat(salt));
    }

    public static Boolean validatePassword(String password, String salt, String hash) {
        String actual = DigestUtils.md5Hex(password.concat(salt));
        return actual.equals(hash);
    }
}
