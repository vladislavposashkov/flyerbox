package com.flyerbox.util;

import com.flyerbox.dao.UserDAO;
import com.flyerbox.model.Answer;
import org.json.simple.JSONObject;

import java.util.List;
import java.util.logging.Logger;

public class JsonUtill {

    private final static Logger LOGGER = Logger.getLogger(UserDAO.class.getName());

    public static JSONObject prepareAnswerFromList(List<Answer> answerList) {

        JSONObject answerJson = new JSONObject();
        String template = "answer_";
        Integer counter = 1;


        for (int i = 0; i < answerList.size(); ++i) {
            String answerNumber = template.concat(counter.toString());
            JSONObject answerDetail = new JSONObject();
            answerDetail.put("id", answerList.get(i).getId());
            answerDetail.put("text", answerList.get(i).getText());
            answerJson.put(answerNumber, answerDetail);
            counter++;
        }
        LOGGER.info("JsonUtill Resault: " + answerJson.toJSONString());
        return answerJson;
    }
}
