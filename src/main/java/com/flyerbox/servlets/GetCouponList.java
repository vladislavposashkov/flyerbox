package com.flyerbox.servlets;

import com.flyerbox.dao.Factory;
import com.flyerbox.dao.imlp.LocationDAOImpl;
import com.flyerbox.util.HibernateUtil;
import com.flyerbox.util.ServleteUtill;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Logger;

@WebServlet("/getCouponList")
public class GetCouponList extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=UTF-8");
        PrintWriter printWriter = resp.getWriter();
        JSONObject resault = new JSONObject();
        String status = "null";
        Session session = null;
        try {
            if (req.getParameter("token") == null) {
                throw new IllegalArgumentException();
            }
            Integer userId = Factory.getUserSessionDAO().getUserIdByToken(req.getParameter("token"));

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery("select c.id, c.description, c.discount, c.expiryDate, u.used from Coupon c, UserCoupon u where u.userId = :userId and c.id = u.couponId");
            query.setParameter("userId", userId);


            List list = query.list();
            LOGGER.info("SIZE " + list.size());

            String template = "coupon_";
            Integer count = 1;
            JSONObject coupons = new JSONObject();
            for (Object rows : list) {
                JSONObject detail = new JSONObject();
                Object[] row = (Object[]) rows;
                String field1 = row[0].toString();
                String field2 = row[1].toString();
                String field3 = row[2].toString();
                String field4 = row[3].toString();
                String field5 = row[4].toString();

                detail.put("id", field1);
                detail.put("description", field2);
                detail.put("discount", field3);
                detail.put("expiryDate", field4);
                detail.put("used", field5);
                String name = template.concat(count.toString());
                count++;
                coupons.put(name, detail);
            }


            resault.put("coupons", coupons);
            status = ServleteUtill.STATUS_OK;

        } catch (HibernateException he) {
            resault.put("Hibernate error", he.getMessage());
            status = "error";
        } catch (Exception e) {
            resault.put("errorCode", e.getMessage());
            status = "error";
        } finally {
            if (session != null) {
                session.close();
            }
            resault.put("status", status);
            printWriter.print(resault);
        }
    }
}
