package com.flyerbox.servlets;

import com.flyerbox.dao.Factory;
import com.flyerbox.model.Location;
import com.flyerbox.model.User;
import org.hibernate.HibernateException;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by vladislav on 29.10.14.
 */
@WebServlet("/info")
public class Info extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=UTF-8");
        PrintWriter printWriter = resp.getWriter();
        JSONObject jsonObject = new JSONObject();
        String status = "null";
        try {
            if (!Factory.getInstance().getUserDAO().isEmailExist(
                    req.getParameter("email"))) {
                status = "error";
                jsonObject.put("description", "Email not already exist");
            } else {
                User user = Factory.getInstance().getUserDAO()
                        .getUserByEmail(req.getParameter("email"));
                Location location = Factory.getInstance()
                        .getLocationDAO()
                        .getLocationById(user.getLocationId());
                status = "success";
                jsonObject.put("firstName", user.getFirstName());
                jsonObject.put("lastName", user.getLastName());
                jsonObject.put("city", location.getCity());
                jsonObject.put("country", location.getCountry());
                jsonObject.put("email", user.getEmail());
            }
        } catch (HibernateException he) {
            jsonObject.put("Hibernate error", he.getMessage());
            status = "error";
        } catch (Exception e) {
            jsonObject.put("errorCode", e.getMessage());
            status = "error";
        } finally {
            jsonObject.put("status", status);
            printWriter.print(jsonObject);
            printWriter.flush();
        }
    }
}
