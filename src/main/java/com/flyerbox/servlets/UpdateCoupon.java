package com.flyerbox.servlets;


import com.flyerbox.dao.Factory;
import com.flyerbox.dao.imlp.LocationDAOImpl;
import com.flyerbox.model.UserCoupon;
import com.flyerbox.util.ServleteUtill;
import org.hibernate.HibernateException;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet("/updateCoupon")
public class UpdateCoupon extends HttpServlet {
    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json; charset=UTF-8");
        JSONObject resault = new JSONObject();
        String status = ServleteUtill.STATUS_ERROR;
        try {
            if (req.getParameter("couponId") == null ||
                    req.getParameter("token") == null) {
                throw new IllegalArgumentException();
            }

            Integer userId = Factory.getUserSessionDAO().getUserIdByToken(req.getParameter("token"));
            LOGGER.info("USER ID " + userId);
            Integer couponID = Integer.parseInt(req.getParameter("couponId"));
            UserCoupon userCoupon = Factory.getUserCouponDAO().getUserCouponFromCouponId(couponID);

            LOGGER.info("USER ID real " + userId + " COUPON " + userCoupon.getUserId());
            if (!userCoupon.getUserId().equals(userId)) {
                throw new Exception("Coupon not found");
            }

            userCoupon.setUsed(true);
            Factory.getUserCouponDAO().updateUserCoupon(userCoupon);
            status = ServleteUtill.STATUS_OK;
        } catch (HibernateException ex) {
            LOGGER.warning(ex.getMessage());
        } catch (IllegalArgumentException ex) {
            LOGGER.warning(ex.getMessage());
        } catch (Exception e) {
            resault.put("desc", "Coupon not found");
            e.printStackTrace();
        } finally {
            resault.put("status", status);
            resp.getWriter().println(resault);
        }
    }
}
