package com.flyerbox.servlets;


import com.flyerbox.dao.Factory;
import com.flyerbox.dao.imlp.LocationDAOImpl;
import com.flyerbox.model.Coupon;
import com.flyerbox.model.UserAnswer;
import com.flyerbox.model.UserCoupon;
import com.flyerbox.model.UserSurvey;
import org.hibernate.HibernateException;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

@WebServlet("/saveSurvey")
public class SaveSurvey extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json; charset=UTF-8");
        JSONObject ourResponse = new JSONObject();
        String status = "error";

        try {

            if (req.getParameter("answers") == null ||
                    req.getParameter("survey_id") == null ||
                    req.getParameter("token") == null) {
                throw new IllegalArgumentException();
            }


            Integer userId = Factory.getUserSessionDAO().getUserIdByToken(req.getParameter("token"));


            Integer surveyId = Integer.parseInt(req.getParameter("survey_id"));

            UserSurvey userSurvey = new UserSurvey();
            userSurvey.setUserId(userId);
            userSurvey.setSurveyId(surveyId);
            userSurvey.setIsAnswered(true);
            Factory.getUserSurveyDAO().addUserSurvey(userSurvey);

            JSONObject answersJson = (JSONObject) JSONValue.parse(req.getParameter("answers"));


            Set questionKeySet = answersJson.entrySet();
            Iterator questionsKeySetIterator = questionKeySet.iterator();


            while (questionsKeySetIterator.hasNext()) {
                Map.Entry question = (Map.Entry) questionsKeySetIterator.next();
                if (question.getKey().toString().contains("question_")) {
                    Map questionDetail = (Map) question.getValue();
                    Integer questionId = Integer.parseInt(questionDetail.get("question_id").toString());
                    Integer answerId = Integer.parseInt(questionDetail.get("answer_id").toString());
                    UserAnswer userAnswer = new UserAnswer(userId, surveyId, questionId, answerId);
                    Factory.getUserAnswerDAO().addNewUserAnswer(userAnswer);
                }
            }

            Coupon coupon = Factory.getCouponDAO().getCouponFromSurveyId(surveyId);
            coupon.setIssued(true);
            Factory.getCouponDAO().updateCoupon(coupon);
            Factory.getUserCouponDAO().addUserCoupon(new UserCoupon(userId, coupon.getId(), false));
            status = "success";
        } catch (HibernateException ex) {
            LOGGER.warning(ex.getMessage());
        } catch (IndexOutOfBoundsException ex) {
            LOGGER.info("No coupons");
        } catch (IllegalArgumentException ex) {
            LOGGER.warning("IllegalArgumentException");
            LOGGER.info("Params answers: " + req.getParameter("answers") +
                    "survey_id : " + req.getParameter("survey_id") +
                    "token: " + req.getParameter("token"));
            ourResponse.put("desc", "Illegal Argument");
        } finally {
            ourResponse.put("status", status);
            resp.getWriter().print(ourResponse);
        }
    }
}
