package com.flyerbox.servlets;

import com.flyerbox.dao.Factory;
import com.flyerbox.model.Survey;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/getSurveyList")
public class GetSurveyList extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=UTF-8");
        List<Survey> surveys;
        PrintWriter printWriter = resp.getWriter();
        String status = "error";
        JSONObject jsonObject = new JSONObject();

        try {
            log("token" + req.getParameter("token"));
            log("getToken");
            Integer userId = Factory.getUserSessionDAO().getUserIdByToken(req.getParameter("token"));
            log("User id" + userId);
            List<Integer> answeredSurveyId = Factory.getUserSurveyDAO().getUserSurveyList(userId);
            surveys = Factory.getSurveyDAO().getAllSurvey();
            String template = "survey_";
            Integer counter = 1;
            for (int i = 0; i < surveys.size(); ++i) {
                String isAnswered = answeredSurveyId.contains(surveys.get(i).getId()) ? "true" : "false";
                log("is ANSWERED" + isAnswered);
                JSONObject temp = new JSONObject();
                temp.put("id", surveys.get(i).getId());
                temp.put("description", surveys.get(i).getDescription());
                temp.put("answered", isAnswered);

                String name = template.concat(counter.toString());
                jsonObject.put(name, temp);
                counter++;
            }
            status = "success";
        } catch (Exception ex) {
            log("Error");
        } finally {
            jsonObject.put("status", status);
            printWriter.print(jsonObject);
        }
    }
}
