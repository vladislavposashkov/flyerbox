package com.flyerbox.servlets;


import com.flyerbox.dao.Factory;
import com.flyerbox.model.Answer;
import com.flyerbox.model.Coupon;
import com.flyerbox.model.Question;
import com.flyerbox.model.Survey;
import org.hibernate.HibernateException;
import org.json.simple.JSONObject;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet("/createSurvey")
public class CreateSurvey extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=UTF-8");
        PrintWriter printWriter = resp.getWriter();
        JSONObject jsonObject = new JSONObject();
        String status = "null";
        try {
            Survey survey = new Survey();
            survey.setIsOpen(true);
            survey.setStartDate(Calendar.getInstance().getTime());
            survey.setEndDate(Calendar.getInstance().getTime());

            JSONParser parser = new JSONParser();
            ContainerFactory containerFactory = new ContainerFactory() {
                public List creatArrayContainer() {
                    return new LinkedList();
                }

                public Map createObjectContainer() {
                    return new LinkedHashMap();
                }
            };


            Map json = (Map) parser.parse(req.getParameter("survey"), containerFactory);
            Iterator surveyIterator = json.entrySet().iterator();

            survey.setDescription(json.get("surveyDescription").toString());

            Factory.getSurveyDAO().addSurvey(survey);
            while (surveyIterator.hasNext()) {
                Map.Entry entry = (Map.Entry) surveyIterator.next();
                if (entry.getKey().toString().contains("question")) {

                    Map<String, String> innerEntry = (Map) entry.getValue();
                    Question question = new Question(survey.getId(),
                            innerEntry.get("question"), innerEntry.get("type"));

                    Factory.getQuestionDAO().addQuestion(question);
                    for (Map.Entry value : innerEntry.entrySet())
                        if (value.getKey().toString().contains("answer")) {
                            Factory.getAnswerDAO().addAnswer(new Answer(
                                    question.getId(),
                                    value.getValue().toString()));
                        }
                }
            }

            for (int i = 0; i < 25; i++) {
                Coupon coupon = new Coupon();
                coupon.setDiscount(10);
                java.sql.Date date = java.sql.Date.valueOf("2014-12-28");
                coupon.setExpiryDate(date);
                coupon.setIssued(false);
                coupon.setSurveyId(survey.getId());
                coupon.setDescription(survey.getDescription());
                Factory.getCouponDAO().addCoupon(coupon);
            }

            status = "success";
        } catch (HibernateException he) {
            jsonObject.put("Hibernate error", he.getMessage());
            status = "error";
        } catch (Exception e) {
            jsonObject.put("errorCode", e.getMessage());
            status = "error";
        } finally {
            jsonObject.put("status", status);
            printWriter.print(jsonObject);
            printWriter.flush();
        }
    }
}
