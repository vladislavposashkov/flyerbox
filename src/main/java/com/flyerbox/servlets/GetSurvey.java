package com.flyerbox.servlets;


import com.flyerbox.dao.Factory;
import com.flyerbox.model.Answer;
import com.flyerbox.model.Question;
import com.flyerbox.model.Survey;
import com.flyerbox.util.JsonUtill;
import org.hibernate.HibernateException;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/getSurvey")
public class GetSurvey extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json; charset=UTF-8");
        JSONObject ourResponse = new JSONObject();
        String status = "error";

        try {
            Integer surveyIdFromReq = Integer.parseInt(req.getParameter("survey"));
            JSONObject questionsJson = new JSONObject();

            log("Info: get survey");
            Survey survey = Factory.getSurveyDAO().getSurveyById(surveyIdFromReq);

            ourResponse.put("survey_id", survey.getId());
            ourResponse.put("survey_description", survey.getDescription());

            log("Info: get question");
            List<Question> questions = Factory.getQuestionDAO().getQuestionsForSurvey(survey.getId());

            String template = "question_";
            Integer counter = 1;
            for (int i = 0; i < questions.size(); i++) {
                log("Create question " + counter);
                JSONObject questionDetailJson = new JSONObject();

                questionDetailJson.put("question_id", questions.get(i).getId());
                questionDetailJson.put("question_text", questions.get(i).getText());

                log("get AnswerList");
                List<Answer> answerList = Factory.getAnswerDAO().getAnswerForQuestion(questions.get(i).getId());
                log("prepare JSON Anwer");
                JSONObject answersJson = JsonUtill.prepareAnswerFromList(answerList);
                questionDetailJson.put("answers", answersJson);
                String questionIndex = template.concat(counter.toString());

                questionsJson.put(questionIndex, questionDetailJson);
                counter++;
            }
            ourResponse.put("questions", questionsJson);
            status = "success";
        } catch (NumberFormatException ex) {
            log("Invalid survey id value");
        } catch (HibernateException ex) {
            log("Invalid value", ex.getCause());
        } finally {
            ourResponse.put("status", status);
            resp.getWriter().print(ourResponse);
        }
    }
}
