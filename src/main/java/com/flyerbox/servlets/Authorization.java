package com.flyerbox.servlets;

import com.flyerbox.dao.Factory;
import com.flyerbox.model.User;
import com.flyerbox.model.UserSession;
import com.flyerbox.util.PasswordUtil;
import org.hibernate.HibernateException;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;


@WebServlet("/authorization")
public class Authorization extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=UTF-8");
        PrintWriter printWriter = resp.getWriter();
        JSONObject jsonObject = new JSONObject();
        String status = "null";
        try {
            if (!Factory.getInstance().getUserDAO().isEmailExist(
                    req.getParameter("email"))) {
                status = "error";
                jsonObject.put("description", "Email not already exist");
            } else {
                User user = Factory.getInstance().getUserDAO()
                        .getUserByEmail(req.getParameter("email"));

                status = PasswordUtil.validatePassword(
                        req.getParameter("password"),
                        user.getSalt(),
                        user.getPassword()
                ) ? "success" : "error";

                if (status != "error") {
                    Random random = new Random();
                    Integer token = Math.abs(random.nextInt());
                    UserSession userSession = new UserSession();
                    userSession.setUserId(user.getId());
                    userSession.setToken(token.toString());
                    Factory.getUserSessionDAO().addSession(userSession);
                    jsonObject.put("token", token.toString());
                }
            }
        } catch (HibernateException he) {
            jsonObject.put("Hibernate error", he.getMessage());
            status = "error";
        } catch (Exception e) {
            jsonObject.put("errorCode", e.getMessage());
            status = "error";
        } finally {
            jsonObject.put("status", status);
            printWriter.print(jsonObject);
            printWriter.flush();
        }
    }
}
