package com.flyerbox.servlets;

import com.flyerbox.dao.Factory;
import com.flyerbox.model.Location;
import com.flyerbox.model.User;
import com.flyerbox.util.PasswordUtil;
import org.hibernate.HibernateException;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/registration")
public class Registration extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=UTF-8");
        PrintWriter printWriter = resp.getWriter();
        JSONObject jsonObject = new JSONObject();

        String status = "null";
        try {

            if (Factory.getInstance().getUserDAO().isEmailExist(
                    req.getParameter("email"))) {
                status = "error";
                jsonObject.put("description", "Email already exist");
            } else {

                User user = new User(req.getParameter("firstName"),
                        req.getParameter("lastName"),
                        req.getParameter("email"));

                Location location = new Location();
                location.setCountry(req.getParameter("country"));
                location.setCity(req.getParameter("city"));

                Factory.getInstance().getLocationDAO().addLocation(location);


                user.setLocationId(location.getId());
                user.setSalt(PasswordUtil.createSalt());
                user.setPassword(PasswordUtil.createHash(
                        req.getParameter("password"),
                        user.getSalt()
                ));

                Factory.getInstance().getUserDAO().addUser(user);
                status = "success";
            }
        } catch (HibernateException he) {
            jsonObject.put("Hibernate error", he.getMessage());
            status = "error";
        } catch (Exception e) {
            jsonObject.put("errorCode", e.getMessage());
            status = "error";
        } finally {
            jsonObject.put("status", status);
            printWriter.print(jsonObject);
            printWriter.flush();
        }
    }
}
