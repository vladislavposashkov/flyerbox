package com.flyerbox.dao;


import com.flyerbox.model.Survey;
import org.hibernate.HibernateException;

import java.util.List;

public interface SurveyDAO {

    public void addSurvey(Survey user) throws HibernateException;

    public Survey getSurveyById(Integer id) throws HibernateException;

    public List<Survey> getAllSurvey() throws HibernateException;
}
