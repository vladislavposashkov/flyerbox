package com.flyerbox.dao;


import com.flyerbox.model.UserSurvey;
import org.hibernate.HibernateException;

import java.util.List;

public interface UserSurveyDAO {

    public void addUserSurvey(UserSurvey userSurvey) throws HibernateException;

    public List<Integer> getUserSurveyList(Integer userId) throws HibernateException;

}
