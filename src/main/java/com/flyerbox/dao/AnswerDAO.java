package com.flyerbox.dao;


import com.flyerbox.model.Answer;
import org.hibernate.HibernateException;

import java.util.List;

public interface AnswerDAO {
    public void addAnswer(Answer answer) throws HibernateException;

    public List<Answer> getAnswerForQuestion(Integer id) throws HibernateException;
}
