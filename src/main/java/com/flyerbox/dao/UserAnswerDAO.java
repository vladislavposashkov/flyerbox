package com.flyerbox.dao;


import com.flyerbox.model.UserAnswer;
import org.hibernate.HibernateException;

public interface UserAnswerDAO {

    public void addNewUserAnswer(UserAnswer userAnswer) throws HibernateException;

}
