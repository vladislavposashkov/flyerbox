package com.flyerbox.dao;

import com.flyerbox.model.Location;

import java.sql.SQLException;
import java.util.List;

public interface LocationDAO {
    public void addLocation(Location location) throws SQLException;

    public List<Location> getAllLocation() throws SQLException;

    public Location getLocationById(Integer id) throws SQLException;

    public Integer getIdByCity(String city) throws SQLException;
}
