package com.flyerbox.dao.imlp;

import com.flyerbox.dao.UserAnswerDAO;
import com.flyerbox.model.UserAnswer;
import com.flyerbox.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.logging.Logger;


public class UserAnswerDAOImpl implements UserAnswerDAO {

    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    public void addNewUserAnswer(UserAnswer userAnswer) throws HibernateException {

        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(userAnswer);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

    }

}
