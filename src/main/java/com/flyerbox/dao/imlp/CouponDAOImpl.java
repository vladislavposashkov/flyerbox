package com.flyerbox.dao.imlp;

import com.flyerbox.dao.CouponDAO;
import com.flyerbox.model.Coupon;
import com.flyerbox.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;
import java.util.logging.Logger;

public class CouponDAOImpl implements CouponDAO {

    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    public void addCoupon(Coupon coupon) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(coupon);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateCoupon(Coupon coupon) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(coupon);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public Coupon getCouponFromSurveyId(Integer id) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Coupon where surveyId = :surveyId and issued = :issued")
                    .setParameter("surveyId", id)
                    .setParameter("issued", false);
            List<Coupon> couponList = (List<Coupon>) query.list();
            LOGGER.warning("SIZE " + couponList.size());
            return couponList.get(0);
        } catch (HibernateException ex) {
            LOGGER.warning(ex.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return new Coupon();
    }

    public List<Coupon> getCouponListByIds(List<Integer> idList) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Coupon where id in :idList").setParameter("idList", idList);
            List<Coupon> couponList = query.list();
            LOGGER.info("Count " + couponList.size());
            return couponList;
        } catch (HibernateException ex) {
            LOGGER.warning(ex.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return null;
    }

    public Coupon getCouponFromId(Integer id) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Coupon where id = :id").setParameter("id", id);
            return (Coupon) query.uniqueResult();
        } catch (HibernateException ex) {
            LOGGER.warning(ex.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return null;
    }
}
