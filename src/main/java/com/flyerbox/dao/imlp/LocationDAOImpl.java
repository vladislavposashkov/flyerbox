package com.flyerbox.dao.imlp;

import com.flyerbox.dao.LocationDAO;
import com.flyerbox.model.Location;
import com.flyerbox.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LocationDAOImpl implements LocationDAO {

    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    public void addLocation(Location location) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            if (isCityExist(location.getCity())) {
                Query query = session.createQuery("select id from Location where city = :city")
                        .setParameter("city", location.getCity());
                if (query.uniqueResult() == null) {
                    throw new HibernateException("Error in read location id");
                }
                location.setId((Integer) query.uniqueResult());
                return;
            }
            session.beginTransaction();
            session.save(location);
            session.getTransaction().commit();
            LOGGER.info("ADD CITY");
        } catch (HibernateException tx) {
            if (session != null) {
                session.getTransaction().rollback();
                LOGGER.log(Level.SEVERE, "Exception: error adding locations", tx.getMessage());
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public Location getLocationById(Integer id) throws SQLException {
        Session session = null;
        Location location = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Location where id = :id");
            query.setParameter("id", id);
            location = (Location) query.uniqueResult();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return location;
    }

    private boolean isCityExist(String city) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            LOGGER.info("into isCityExist");
            Query query = session.createQuery("select id from Location  where city = :city");
            query.setParameter("city", city);
            return query.uniqueResult() != null;
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return false;
    }

    public Integer getIdByCity(String cityName) throws SQLException {
        Session session = null;
        Integer locationId = null;

        try {
            LOGGER.log(Level.INFO, "Get City in DB");
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("select id from Location where city = :city");
            query.setParameter("city", cityName);
            locationId = (Integer) query.uniqueResult();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return locationId;
    }

    public List<Location> getAllLocation() throws SQLException {
        Session session = null;
        List<Location> locations = new ArrayList<Location>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            locations = session.createCriteria(Location.class).list();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return locations;
    }

}
