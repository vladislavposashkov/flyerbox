package com.flyerbox.dao.imlp;

import com.flyerbox.dao.UserSessionDAO;
import com.flyerbox.model.UserSession;
import com.flyerbox.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.logging.Logger;


public class UserSessionDAOImpl implements UserSessionDAO {

    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    public void addSession(UserSession userSession) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(userSession);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public Integer getUserIdByToken(String token) throws HibernateException {
        Session session = null;
        try {
            LOGGER.info("Step in to getUserId by Token");
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from UserSession where token = :token");
            query.setParameter("token", token);
            UserSession userSession = (UserSession) query.uniqueResult();
            LOGGER.info("User ID = " + userSession.getUserId());
            return userSession.getUserId();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
            }
            LOGGER.warning("Error in getUserIdByToken");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return null;
    }

}
