package com.flyerbox.dao.imlp;

import com.flyerbox.dao.QuestionDAO;
import com.flyerbox.model.Question;
import com.flyerbox.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;
import java.util.logging.Logger;


public class QuestionDAOImpl implements QuestionDAO {

    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    public void addQuestion(Question question) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(question);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
                throw ex;
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public List<Question> getQuestionsForSurvey(Integer id) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Question where surveyId = :id");
            query.setParameter("id", id);
            return query.list();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
                throw ex;
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return null;
    }
}
