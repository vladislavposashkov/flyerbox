package com.flyerbox.dao.imlp;

import com.flyerbox.dao.UserSurveyDAO;
import com.flyerbox.model.UserSurvey;
import com.flyerbox.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;
import java.util.logging.Logger;


public class UserSurveyDAOImpl implements UserSurveyDAO {

    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    public void addUserSurvey(UserSurvey userSurvey) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(userSurvey);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            if (session != null && session.isOpen()) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public List<Integer> getUserSurveyList(Integer userId) throws HibernateException {
        Session session = null;
        List<Integer> userSurveyList;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("select surveyId from UserSurvey where userId = :userId").setParameter("userId", userId);
            userSurveyList = query.list();
            return userSurveyList;
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return null;
    }
}
