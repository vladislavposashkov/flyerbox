package com.flyerbox.dao.imlp;

import com.flyerbox.dao.SurveyDAO;
import com.flyerbox.model.Survey;
import com.flyerbox.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class SurveyDAOImpl implements SurveyDAO {

    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    public void addSurvey(Survey survey) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(survey);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
                throw ex;
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public Survey getSurveyById(Integer id) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Survey where id = :id");
            query.setParameter("id", id);
            System.out.println(query);
            return (Survey) query.uniqueResult();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
                throw ex;
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return null;
    }

    public List<Survey> getAllSurvey() throws HibernateException {
        Session session = null;
        List<Survey> surveys = new ArrayList<Survey>();
        try {
            LOGGER.info("INFO: SurveyDAO Get list of Survey");
            session = HibernateUtil.getSessionFactory().openSession();
            surveys = session.createCriteria(Survey.class).list();
        } catch (HibernateException ex) {
            LOGGER.warning("Error in getAllSurvey");
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return surveys;
    }
}
