package com.flyerbox.dao.imlp;

import com.flyerbox.dao.AnswerDAO;
import com.flyerbox.model.Answer;
import com.flyerbox.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;
import java.util.logging.Logger;


public class AnswerDAOImpl implements AnswerDAO {

    private final static Logger LOGGER = Logger.getLogger(LocationDAOImpl.class.getSimpleName());

    public void addAnswer(Answer answer) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(answer);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
                throw ex;
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public List<Answer> getAnswerForQuestion(Integer questionId) throws HibernateException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Answer where questionId = :id");
            query.setParameter("id", questionId);
            return query.list();
        } catch (HibernateException ex) {
            if (session != null) {
                session.getTransaction().rollback();
                throw ex;
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return null;
    }
}
