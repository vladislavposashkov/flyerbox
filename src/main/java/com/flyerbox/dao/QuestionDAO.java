package com.flyerbox.dao;

import com.flyerbox.model.Question;
import org.hibernate.HibernateException;

import java.util.List;

public interface QuestionDAO {
    public void addQuestion(Question question) throws HibernateException;

    public List<Question> getQuestionsForSurvey(Integer id) throws HibernateException;
}
