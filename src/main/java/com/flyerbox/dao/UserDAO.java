package com.flyerbox.dao;

import com.flyerbox.model.User;

import java.sql.SQLException;

public interface UserDAO {
    public void addUser(User location) throws SQLException, Exception;

    public User getUserByEmail(String email) throws SQLException;

    public boolean isEmailExist(String email) throws SQLException;
}
