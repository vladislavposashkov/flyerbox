package com.flyerbox.dao;

import com.flyerbox.model.UserCoupon;
import org.hibernate.HibernateException;

import java.util.List;


public interface UserCouponDAO {
    public void addUserCoupon(UserCoupon userCoupon) throws HibernateException;

    public void updateUserCoupon(UserCoupon userCoupon) throws HibernateException;

    public UserCoupon getUserCouponById(Integer id) throws HibernateException;

    public UserCoupon getUserCouponFromCouponId(Integer id) throws HibernateException;

    public List<UserCoupon> getUserCouponList(Integer userId) throws HibernateException;

}
