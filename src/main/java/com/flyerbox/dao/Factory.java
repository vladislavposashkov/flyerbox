package com.flyerbox.dao;

import com.flyerbox.dao.imlp.*;

public class Factory {

    private static AnswerDAO answerDAO = null;
    private static LocationDAO locationDAO = null;
    private static QuestionDAO questionDAO = null;
    private static SurveyDAO surveyDAO = null;
    private static UserDAO userDAO = null;
    private static UserAnswerDAO userAnswerDAO = null;
    private static UserSessionDAO userSessionDAO = null;
    private static UserSurveyDAO userSurveyDAO = null;
    private static UserCouponDAO userCouponDAO = null;
    private static CouponDAO couponDAO = null;
    private static Factory instance = null;


    public static synchronized Factory getInstance() {
        if (instance == null) {
            instance = new Factory();
        }
        return instance;
    }

    public static AnswerDAO getAnswerDAO() {
        if (answerDAO == null) {
            answerDAO = new AnswerDAOImpl();
        }
        return answerDAO;
    }

    public static QuestionDAO getQuestionDAO() {
        if (questionDAO == null) {
            questionDAO = new QuestionDAOImpl();
        }
        return questionDAO;
    }

    public static SurveyDAO getSurveyDAO() {
        if (surveyDAO == null) {
            surveyDAO = new SurveyDAOImpl();
        }
        return surveyDAO;
    }

    public static UserAnswerDAO getUserAnswerDAO() {
        if (userAnswerDAO == null) {
            userAnswerDAO = new UserAnswerDAOImpl();
        }
        return userAnswerDAO;
    }

    public static UserSurveyDAO getUserSurveyDAO() {
        if (userSurveyDAO == null) {
            userSurveyDAO = new UserSurveyDAOImpl();
        }
        return userSurveyDAO;
    }

    public static LocationDAO getLocationDAO() {
        if (locationDAO == null) {
            locationDAO = new LocationDAOImpl();
        }
        return locationDAO;
    }

    public static UserDAO getUserDAO() {
        if (userDAO == null) {
            userDAO = new UserDAOImpl();
        }
        return userDAO;
    }

    public static UserSessionDAO getUserSessionDAO() {
        if (userSessionDAO == null) {
            userSessionDAO = new UserSessionDAOImpl();
        }
        return userSessionDAO;
    }

    public static UserCouponDAO getUserCouponDAO() {
        if (userCouponDAO == null) {
            userCouponDAO = new UserCouponDAOImpl();
        }
        return userCouponDAO;
    }

    public static CouponDAO getCouponDAO() {
        if (couponDAO == null) {
            couponDAO = new CouponDAOImpl();
        }
        return couponDAO;
    }
}
