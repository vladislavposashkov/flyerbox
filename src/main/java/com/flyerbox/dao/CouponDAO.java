package com.flyerbox.dao;


import com.flyerbox.model.Coupon;
import org.hibernate.HibernateException;

import java.util.List;


public interface CouponDAO {
    public void addCoupon(Coupon coupon) throws HibernateException;

    public void updateCoupon(Coupon coupon) throws HibernateException;

    public Coupon getCouponFromSurveyId(Integer id) throws HibernateException;

    public List<Coupon> getCouponListByIds(List<Integer> idList) throws HibernateException;

    public Coupon getCouponFromId(Integer id) throws HibernateException;
}
