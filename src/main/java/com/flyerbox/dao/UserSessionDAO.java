package com.flyerbox.dao;


import com.flyerbox.model.UserSession;
import org.hibernate.HibernateException;

public interface UserSessionDAO {
    public void addSession(UserSession userSession) throws HibernateException;

    public Integer getUserIdByToken(String token) throws HibernateException;
}
