package com.flyerbox.model;

import javax.persistence.*;

@Entity
@Table(name = "fb_user_session")
public class UserSession {
    private Integer id;
    private Integer userId;
    private String token;

    @Id
    @Column(name = "session_id", nullable = false)
    @SequenceGenerator(name = "session_seq", sequenceName = "fb_user_session_session_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "session_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "user_id", nullable = false)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "token", nullable = false)
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}