package com.flyerbox.model;


import javax.persistence.*;

@Entity
@Table(name = "fb_question")
public class Question {
    Integer id;
    Integer surveyId;
    String text;
    String type;

    public Question() {
    }

    public Question(Integer surveyId, String text, String type) {
        this.surveyId = surveyId;
        this.text = text;
        this.type = type;
    }

    @Id
    @Column(name = "question_id", nullable = false)
    @SequenceGenerator(name = "question_seq", sequenceName = "fb_question_question_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "question_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "survey_id", nullable = false)
    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    @Column(name = "text", nullable = false)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Column(name = "type", nullable = false)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
