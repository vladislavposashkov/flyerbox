package com.flyerbox.model;

import javax.persistence.*;

@Entity
@Table(name = "fb_user")
public class User {

    private Integer id;
    private Integer locationId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Integer rank;
    private Boolean isCompany;
    private String salt;

    public User() {
    }

    public User(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    @Id
    @Column(name = "user_id", nullable = false)
    @SequenceGenerator(name = "user_seq", sequenceName = "fb_user_user_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "location_id", nullable = false)
    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    @Column(name = "first_name", nullable = false)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name", nullable = false)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "email", unique = true, nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "password", nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "rank", columnDefinition = "integer DEFAULT 0")
    public Integer getRank() {
        return (this.rank == null) ? 0 : rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Column(name = "is_company", columnDefinition = "bool default false")
    public Boolean getIsCompany() {
        return isCompany;
    }

    public void setIsCompany(Boolean isCompany) {
        this.isCompany = isCompany;
    }

    @Column(name = "salt", length = 255)
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
