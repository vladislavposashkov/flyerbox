package com.flyerbox.model;

import javax.persistence.*;

@Entity
@Table(name = "fb_location")
public class Location {
    private Integer id;
    private String country;
    private String city;

    @Id
    @Column(name = "location_id", nullable = false)
    @SequenceGenerator(name = "location_seq", sequenceName = "fb_location_location_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "location_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "country", nullable = false)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "city", nullable = false)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
