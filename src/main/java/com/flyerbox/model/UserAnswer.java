package com.flyerbox.model;

import javax.persistence.*;

@Entity
@Table(name = "fb_user_answer")
public class UserAnswer {
    Integer id;
    Integer userId;
    Integer surveyId;
    Integer questionId;
    Integer answerId;

    public UserAnswer() {

    }

    public UserAnswer(Integer userId, Integer surveyId, Integer questionId, Integer answerId) {
        this.userId = userId;
        this.surveyId = surveyId;
        this.questionId = questionId;
        this.answerId = answerId;
    }

    @Id
    @Column(name = "user_answer_id", nullable = false)
    @SequenceGenerator(name = "user_answer_seq", sequenceName = "fb_user_answer_user_answer_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_answer_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "user_id", nullable = false)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "survey_id")
    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    @Column(name = "question_id")
    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    @Column(name = "answer_id")
    public Integer getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Integer answerId) {
        this.answerId = answerId;
    }


}
