package com.flyerbox.model;

import javax.persistence.*;

@Entity
@Table(name = "fb_user_survey")
public class UserSurvey {
    Integer id;
    Integer surveyId;
    Integer userId;
    Boolean isAnswered;

    @Id
    @Column(name = "user_survey_id", nullable = false)
    @SequenceGenerator(name = "user_survey_seq", sequenceName = "fb_user_survey_user_survey_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_survey_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "survey_id", nullable = false)
    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer survey_id) {
        this.surveyId = survey_id;
    }

    @Column(name = "user_id", nullable = false)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer user_id) {
        this.userId = user_id;
    }

    @Column(name = "is_answered", nullable = false)
    public Boolean getIsAnswered() {
        return isAnswered;
    }

    public void setIsAnswered(Boolean isAnswered) {
        this.isAnswered = isAnswered;
    }
}
