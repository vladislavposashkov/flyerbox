package com.flyerbox.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "fb_survey")
public class Survey {
    Integer id;
    String description;
    Date startDate;
    Date endDate;
    Boolean isOpen;

    @Id
    @Column(name = "survey_id", nullable = false)
    @SequenceGenerator(name = "survey_id", sequenceName = "fb_survey_survey_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "survey_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "description", nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "start_date", nullable = false)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Column(name = "end_date", nullable = false)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Column(name = "is_open")
    public Boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Boolean isOpen) {
        this.isOpen = isOpen;
    }
}
