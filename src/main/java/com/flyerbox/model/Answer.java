package com.flyerbox.model;


import javax.persistence.*;


@Entity
@Table(name = "fb_answer")
public class Answer {
    Integer id;
    Integer questionId;
    String text;

    public Answer() {
    }

    public Answer(Integer questionId, String text) {
        this.questionId = questionId;
        this.text = text;
    }

    @Id
    @Column(name = "answer_id", nullable = false)
    @SequenceGenerator(name = "answer_seq", sequenceName = "fb_answer_answer_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "answer_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "question_id", nullable = false)
    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
