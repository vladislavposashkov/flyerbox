package com.flyerbox.model;

import javax.persistence.*;

@Entity
@Table(name = "fb_user_coupon")
public class UserCoupon {
    Integer id;
    Integer userId;
    Integer couponId;
    Boolean used;

    public UserCoupon() {

    }

    public UserCoupon(Integer userId, Integer couponId, Boolean used) {
        this.userId = userId;
        this.couponId = couponId;
        this.used = used;
    }

    @Id
    @Column(name = "user_coupon_id", nullable = false)
    @SequenceGenerator(name = "user_coupon_seq", sequenceName = "fb_user_coupon_user_coupon_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_coupon_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "coupon_id")
    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    @Column(name = "used")
    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }
}
