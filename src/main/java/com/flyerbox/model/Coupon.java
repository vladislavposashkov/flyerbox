package com.flyerbox.model;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "fb_coupon")
public class Coupon {
    private Integer id;
    private Integer surveyId;
    private String description;
    private Date expiryDate;
    private Integer discount;
    private Boolean issued;


    @Id
    @Column(name = "coupon_id", nullable = false)
    @SequenceGenerator(name = "coupon_seq", sequenceName = "fb_coupon_coupon_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coupon_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "survey_id")
    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "expiry_date")
    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Column(name = "discount")
    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    @Column(name = "issued", columnDefinition = "bool default false")
    public Boolean getIssued() {
        return issued;
    }

    public void setIssued(Boolean issued) {
        this.issued = issued;
    }
}
